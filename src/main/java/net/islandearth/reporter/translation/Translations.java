package net.islandearth.reporter.translation;

import me.clip.placeholderapi.PlaceholderAPI;
import net.islandearth.languagy.api.language.Language;
import net.islandearth.reporter.Reporter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public enum Translations {
	CANNOT_REPORT_SELF("&cYou cannot report yourself!"),
	NO_REASON("&c&oPlease enter a reason."),
	NO_PERMISSION("&cI'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error."),
	REPORT_ENTRY("&bAdding an entry under the name: &3%0"),
	NEW_REPORT("&4&o%0 &chas submitted a new report against &4&o%1&c: &b&o%2"),
	PLAYER_DOES_NOT_EXIST("&cThat player does not exist."),
	ALREADY_REPORTED("&cYou already have an open report against that player."),
	REPORT_CHAT_PROMPT("&aEnter your report reason in chat. &cSay 'cancel' to cancel the report."),
	REPORT_CANCELLED("&cYou have cancelled your report."),
	REPORT_MAIN_MENU("&aReporter > Main Menu"),
	REPORT_MANAGE_MENU("&aReporter > Report Manager"),
	REPORT_PLAYER_MENU("&aReporter > Report Menu"),
	REPORT_REPORTER("&fReporter: %0"),
	REPORT_REPORTED("&fReported: %0"),
	REPORT_REASON("&fReason: %0"),
	REPORT_TOGGLE("&7Left Click to toggle status"),
	REPORT_REMOVE("&7Right Click to remove");

	private final String defaultValue;
	private final boolean isList;
	
	Translations(String defaultValue) {
		this.defaultValue = defaultValue;
		this.isList = false;
	}

	Translations(String defaultValue, boolean isList) {
		this.defaultValue = defaultValue;
		this.isList = isList;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}

	public boolean isList() {
		return isList;
	}

	private String getPath() {
		return this.toString().toLowerCase();
	}

	public void send(Player player) {
		String message = Reporter.getAPI().getTranslator().getTranslationFor(player, this.getPath());
		player.sendMessage(this.setPapi(player, message));
	}

	public void send(Player player, String... values) {
		String message = Reporter.getAPI().getTranslator().getTranslationFor(player, this.getPath());
		message = this.setPapi(player, replaceVariables(message, values));
		player.sendMessage(message);
	}

	public void sendList(Player player) {
		List<String> messages = Reporter.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
		messages.forEach(message -> player.sendMessage(this.setPapi(player, message)));
	}

	public void sendList(Player player, String... values) {
		List<String> messages = Reporter.getAPI().getTranslator().getTranslationListFor(player, this.getPath());
		messages.forEach(message -> {
			message = this.setPapi(player, replaceVariables(message, values));
			player.sendMessage(message);
		});
	}

	public String get(Player player) {
		return this.setPapi(player, Reporter.getAPI().getTranslator().getTranslationFor(player, this.getPath()));
	}
	
	public String get(Player player, String... values) {
		String message = Reporter.getAPI().getTranslator().getTranslationFor(player, this.getPath());
		message = replaceVariables(message, values);
		return this.setPapi(player, message);
	}

	public List<String> getList(Player player) {
		List<String> list = new ArrayList<>();
		Reporter.getAPI().getTranslator().getTranslationListFor(player, this.getPath()).forEach(text -> list.add(this.setPapi(player, text)));
		return list;
	}

	public List<String> getList(Player player, String... values) {
		List<String> messages = new ArrayList<>();
		Reporter.getAPI().getTranslator()
				.getTranslationListFor(player, this.getPath())
				.forEach(message -> messages.add(this.setPapi(player, replaceVariables(message, values))));
		return messages;
	}
	
	public static void generateLang(Reporter plugin) {
		File lang = new File(plugin.getDataFolder() + "/lang/");
		lang.mkdirs();
		
		for (Language language : Language.values()) {
			try {
				plugin.saveResource("lang/" + language.getCode() + ".yml", false);
				plugin.getLogger().info("Generated " + language.getCode() + ".yml");
			} catch (IllegalArgumentException ignored) { }

			File file = new File(plugin.getDataFolder() + "/lang/" + language.getCode() + ".yml");
			if (file.exists()) {
				FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				for (Translations key : values()) {
					if (config.get(key.toString().toLowerCase()) == null) {
						plugin.getLogger().warning("No value in translation file for key "
								+ key.toString() + " was found. Regenerate language files?");
					}
				}
			}
		}
	}

	@NotNull
	private String replaceVariables(String message, String... values) {
		String modifiedMessage = message;
		for (int i = 0; i < 10; i++) {
			if (values.length > i) modifiedMessage = modifiedMessage.replaceAll("%" + i, values[i]);
			else break;
		}

		return modifiedMessage;
	}

	@NotNull
	private String setPapi(Player player, String message) {
		if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			return PlaceholderAPI.setPlaceholders(player, message);
		}

		return message;
	}
}
