package net.islandearth.reporter;

import co.aikar.commands.PaperCommandManager;
import net.islandearth.languagy.api.language.Language;
import net.islandearth.languagy.api.language.LanguagyImplementation;
import net.islandearth.languagy.api.language.LanguagyPluginHook;
import net.islandearth.languagy.api.language.Translator;
import net.islandearth.reporter.commands.Report;
import net.islandearth.reporter.translation.Translations;
import net.islandearth.reporter.utils.FileUtil;
import net.islandearth.reporter.utils.ReportCache;
import net.islandearth.reporter.utils.XMaterial;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public class Reporter extends JavaPlugin implements LanguagyPluginHook {
	
	private Logger log = Bukkit.getLogger();

	@LanguagyImplementation(Language.ENGLISH)
	private Translator translator;
	private boolean isSql = false;

	private Connection sql;
	private String host, database, username, password;
	private int port;

	private ReportCache reportCache;
    
	private static Reporter plugin;

	@Override
	public void onEnable() {
		Reporter.plugin = this;
		createFiles();
		
		if (getConfig().getBoolean("SQL.enabled")) {
			log.info("SQL starting...");
			this.isSql = true;
			host = getConfig().getString("SQL.host");
			port = getConfig().getInt("SQL.port");
			database = getConfig().getString("SQL.database") + getConfig().getString("SQL.variables");
			username = getConfig().getString("SQL.username");
			password = getConfig().getString("SQL.password");
	        
			try {
				openConnection();
				log.info("Connection established!");
				PreparedStatement p1 = sql.prepareStatement("CREATE TABLE IF NOT EXISTS Reports (uuid varchar(32) NOT NULL, date varchar(36) NOT NULL, reporter varchar(36) NOT NULL, reported varchar(36) NOT NULL, reason longtext NOT NULL, status varchar(36) NOT NULL, PRIMARY KEY(uuid))");
				p1.executeUpdate();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
		
		this.reportCache = new ReportCache();
		FileUtil.getReports().forEach(entry -> {
			this.reportCache.getReports().add(entry);
		});

		registerCommands();
		this.hook(this);
	}
	
	@Override
	public void onDisable() {
		reportCache.getReports().forEach(entry -> {
			log.info("Saving entry: " + entry);
			entry.saveEntry();
		});
		Reporter.plugin = null;
		this.reportCache = null;
		this.sql = null;
	}
	
	private void createFiles() {
		File logger = new File(getDataFolder() + "/data/");
		if (!logger.exists()) logger.mkdirs();
		
		File loggerFile = new File(this.getDataFolder() + "/data/logger.yml");
		if (!loggerFile.exists()) {
			try {
				loggerFile.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		File lang = new File(getDataFolder() + "/lang/");
		if (!lang.exists()) lang.mkdirs();

		Translations.generateLang(this);
		
		getConfig().options().copyDefaults(true);
		getConfig().addDefault("SQL.enabled", false);
		getConfig().addDefault("SQL.host", "localhost");
		getConfig().addDefault("SQL.port", 3306);
		getConfig().addDefault("SQL.database", "Reporter");
		getConfig().addDefault("SQL.username", "root");
		getConfig().addDefault("SQL.password", "123");
		getConfig().addDefault("SQL.variables", "?useSSL=false&autoReconnect=true");
		saveConfig();
	}
	
	public void openConnection() throws SQLException, ClassNotFoundException {
	    if (sql != null && !sql.isClosed()) sql = null;
	 
	    synchronized (this) {
	        Class.forName("com.mysql.jdbc.Driver");
	        sql = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
	    }
	}
	
	private void registerCommands() {
		PaperCommandManager manager = new PaperCommandManager(this);
		manager.registerCommand(new Report(this));
	}

	public Translator getTranslator() {
		return translator;
	}

	public boolean isSql() {
		return isSql;
	}

	public ReportCache getReportCache() {
		return reportCache;
	}

	public Connection getSql() {
		return sql;
	}

	public static Reporter getAPI() {
		return plugin;
	}

	@Override
	public void onLanguagyHook() {
		translator.setDisplay(XMaterial.PAPER.parseMaterial(true));
	}
}
