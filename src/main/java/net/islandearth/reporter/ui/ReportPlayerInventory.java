package net.islandearth.reporter.ui;

import com.github.stefvanschie.inventoryframework.Gui;
import com.github.stefvanschie.inventoryframework.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.github.stefvanschie.inventoryframework.pane.util.Mask;
import net.islandearth.reporter.Reporter;
import net.islandearth.reporter.entry.ReportEntry;
import net.islandearth.reporter.entry.ReportEntry.EntryStatus;
import net.islandearth.reporter.translation.Translations;
import net.islandearth.reporter.utils.ItemStackBuilder;
import net.islandearth.reporter.utils.VersionUtil;
import net.islandearth.reporter.utils.XMaterial;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReportPlayerInventory extends ReporterGUI {

    private final Gui gui;

	public ReportPlayerInventory(Reporter plugin, Player player) {
		super(plugin, player);
		this.gui = new Gui(plugin, 5, Translations.REPORT_PLAYER_MENU.get(player));
		gui.setOnGlobalClick(click -> click.setCancelled(true));
		OutlinePane oPane = new OutlinePane(0, 0, 9, 5);
		oPane.setRepeat(true);
		oPane.applyMask(new Mask(
				"111111111",
				"100000001",
				"100000001",
				"100000001",
				"111111111"));
		oPane.setOnClick(inventoryClickEvent -> inventoryClickEvent.setCancelled(true));

		oPane.addItem(new GuiItem(new ItemStackBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseMaterial(true))
				.withName(" ")
				.addFlags(ItemFlag.HIDE_ATTRIBUTES)
				.build()));

		gui.addPane(oPane);

		PaginatedPane pane = new PaginatedPane(1, 2, 7, 1);
		List<GuiItem> guiItems = new ArrayList<>();
		List<ReportEntry> reports = Reporter.getAPI().getReportCache().getReports();
		Bukkit.getOnlinePlayers().forEach(players -> {
			ItemStack item = new ItemStackBuilder(VersionUtil.getSkull(players))
					.withLore(Arrays.asList(ChatColor.GRAY + "[Left Click]", ChatColor.GRAY + players.getUniqueId().toString()))
					.build();
			GuiItem guiItem = new GuiItem(item, inventoryClickEvent -> {
				inventoryClickEvent.setCancelled(true);
				// Check they don't already have an open report against player
				for (ReportEntry entry : reports) {
					if (entry.getReported().getUniqueId().equals(players.getUniqueId())
							&& entry.getReporter().getUniqueId().equals(player.getUniqueId())
							&& entry.getStatus() == EntryStatus.OPEN) {
						Translations.ALREADY_REPORTED.send(player);
						return;
					}
				}

				if (inventoryClickEvent.getCurrentItem() != null
						&& inventoryClickEvent.getCurrentItem().hasItemMeta()
						&& inventoryClickEvent.getCurrentItem().getItemMeta().getLore() != null
						&& !inventoryClickEvent.getCurrentItem().getItemMeta().getLore().contains(ChatColor.GRAY + player.getUniqueId().toString())) {
					new AnvilGUI.Builder()
							.onComplete((responder, text) -> {
								DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
								String time = LocalDateTime.now().format(formatter);
								Reporter.getAPI().getReportCache().getReports().add(new ReportEntry(player, players, text, time, EntryStatus.OPEN));
								Translations.REPORT_ENTRY.send(player, player.getName());
								Bukkit.getOnlinePlayers().forEach(oPlayer -> {
									if (oPlayer.hasPermission("report.view.new")) {
										Translations.NEW_REPORT.send(oPlayer, player.getName(), players.getName(), text);
									}
								});
								return AnvilGUI.Response.close();
							})
							.text("Enter Reason")
							.item(new ItemStack(XMaterial.NAME_TAG.parseMaterial(true)))
							.title("Enter Reason") // only works in 1.14+
							.plugin(plugin)
							.open(player);
				} else {
					Translations.CANNOT_REPORT_SELF.send(player);
				}
			});
			guiItems.add(guiItem);
		});

		pane.populateWithGuiItems(guiItems);

		StaticPane forwardPane = new StaticPane(5, 4, 1 ,1);
		StaticPane previousPane = new StaticPane(3, 4, 1, 1);
		GuiItem next = new GuiItem(new ItemStackBuilder(XMaterial.ARROW.parseMaterial(true))
				.withName(ChatColor.GREEN + "Next Page")
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0 || pane.getPages() == 1) return;

			pane.setPage(pane.getPage() + 1);

			previousPane.setVisible(true);
			gui.update();
		});
		GuiItem previous = new GuiItem(new ItemStackBuilder(XMaterial.ARROW.parseMaterial(true))
				.withName(ChatColor.RED + "Previous Page")
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0 || pane.getPages() == 1) return;

			pane.setPage(pane.getPage() - 1);

			forwardPane.setVisible(true);
			gui.update();
		});
		forwardPane.addItem(next, 0, 0);
		previousPane.addItem(previous, 0, 0);

		gui.addPane(forwardPane);
		gui.addPane(previousPane);
		gui.addPane(pane);
	}

	@Override
	public void open() {
		gui.show(getPlayer());
	}
}