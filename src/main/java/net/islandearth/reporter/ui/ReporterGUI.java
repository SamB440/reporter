package net.islandearth.reporter.ui;

import net.islandearth.reporter.Reporter;
import org.bukkit.entity.Player;

public abstract class ReporterGUI {

	private final Reporter plugin;
	private final Player player;

	public ReporterGUI(Reporter plugin, Player player) {
		this.plugin = plugin;
		this.player = player;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public abstract void open();
}
