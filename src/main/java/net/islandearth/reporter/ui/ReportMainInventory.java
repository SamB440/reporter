package net.islandearth.reporter.ui;

import com.github.stefvanschie.inventoryframework.Gui;
import com.github.stefvanschie.inventoryframework.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import net.islandearth.reporter.Reporter;
import net.islandearth.reporter.translation.Translations;
import net.islandearth.reporter.utils.ItemStackBuilder;
import net.islandearth.reporter.utils.XMaterial;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class ReportMainInventory extends ReporterGUI {

    private final Gui gui;

    public ReportMainInventory(Reporter plugin, Player player) {
		super(plugin, player);
		this.gui = new Gui(plugin, 5, Translations.REPORT_MAIN_MENU.get(player));
		gui.setOnGlobalClick(click -> click.setCancelled(true));

		StaticPane pane = new StaticPane(0, 0, 9, 5);
		pane.fillWith(new ItemStackBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseMaterial(true))
			.addFlags(ItemFlag.HIDE_ATTRIBUTES)
			.withName(" ")
			.build(), inventoryClickEvent -> inventoryClickEvent.setCancelled(true));

		StaticPane pane2 = new StaticPane(4, 2, 1, 1);
		ItemStack report = new ItemStackBuilder(XMaterial.PLAYER_HEAD.parseMaterial(true))
				.withName(ChatColor.RED + "Report a player!")
				.addFlags(ItemFlag.HIDE_ATTRIBUTES)
				.build();
		pane2.addItem(new GuiItem(report, inventoryClickEvent -> {
			inventoryClickEvent.setCancelled(true);
			new ReportPlayerInventory(plugin, player).open();
		}), 0, 0);

		gui.addPane(pane);
		gui.addPane(pane2);
	}

	@Override
	public void open() {
		gui.show(getPlayer());
	}
}