package net.islandearth.reporter.ui;

import com.github.stefvanschie.inventoryframework.Gui;
import com.github.stefvanschie.inventoryframework.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.github.stefvanschie.inventoryframework.pane.util.Mask;
import net.islandearth.reporter.Reporter;
import net.islandearth.reporter.entry.ReportEntry;
import net.islandearth.reporter.translation.Translations;
import net.islandearth.reporter.utils.ItemStackBuilder;
import net.islandearth.reporter.utils.VersionUtil;
import net.islandearth.reporter.utils.XMaterial;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ReportManageInventory extends ReporterGUI {

    private final Gui gui;

	public ReportManageInventory(Reporter plugin, Player player) {
		super(plugin, player);
		this.gui = new Gui(plugin, 5, Translations.REPORT_MANAGE_MENU.get(player));
		gui.setOnGlobalClick(click -> click.setCancelled(true));
		OutlinePane oPane = new OutlinePane(0, 0, 9, 5);
		oPane.setRepeat(true);
		oPane.applyMask(new Mask(
				"111111111",
				"100000001",
				"100000001",
				"100000001",
				"111111111"));
		oPane.setOnClick(inventoryClickEvent -> inventoryClickEvent.setCancelled(true));

		oPane.addItem(new GuiItem(new ItemStackBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseMaterial(true))
				.withName(" ")
				.addFlags(ItemFlag.HIDE_ATTRIBUTES)
				.build()));

		gui.addPane(oPane);

		PaginatedPane pane = new PaginatedPane(1, 2, 7, 1);
		List<ReportEntry> reports = new ArrayList<>(Reporter.getAPI().getReportCache().getReports());
		Collections.reverse(reports);
		List<GuiItem> guiItems = new ArrayList<>();
		reports.forEach(entry -> {
			OfflinePlayer reporter = entry.getReporter();
			OfflinePlayer reported = entry.getReported();
			String reason = entry.getReason();
			ItemStack item = new ItemStackBuilder(VersionUtil.getSkull(reported))
					.withLore(Arrays.asList(
							Translations.REPORT_REPORTER.get(player, reporter.getName()),
							Translations.REPORT_REPORTED.get(player, reporter.getName()),
							Translations.REPORT_REASON.get(player, reason),
							ChatColor.LIGHT_PURPLE + "" + entry.getDate(),
							entry.getStatus().getColour() + entry.getStatus().toString(),
							player.hasPermission("report.toggle") ? Translations.REPORT_TOGGLE.get(player) : "",
							player.hasPermission("report.remove") ? Translations.REPORT_REMOVE.get(player) : ""))
					.build();
			guiItems.add(new GuiItem(item, event -> {
				InventoryAction action = event.getAction();
				switch (action) {
					case PICKUP_HALF:
						if (!player.hasPermission("report.remove")) break;
						entry.deleteEntry();
						player.closeInventory();
						new ReportManageInventory(plugin, player).open();
						break;
					case PICKUP_ALL:
						if (!player.hasPermission("report.toggle")) break;
						entry.setStatus(entry.getStatus().toggle());
						player.closeInventory();
						new ReportManageInventory(plugin, player).open();
						break;
				}
			}));
		});

		pane.populateWithGuiItems(guiItems);

		StaticPane forwardPane = new StaticPane(5, 4, 1 ,1);
		StaticPane previousPane = new StaticPane(3, 4, 1, 1);
		GuiItem next = new GuiItem(new ItemStackBuilder(XMaterial.ARROW.parseMaterial(true))
				.withName(ChatColor.GREEN + "Next Page")
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0 || pane.getPages() == 1) return;

			pane.setPage(pane.getPage() + 1);

			previousPane.setVisible(true);
			gui.update();
		});
		GuiItem previous = new GuiItem(new ItemStackBuilder(XMaterial.ARROW.parseMaterial(true))
			.withName(ChatColor.RED + "Previous Page")
			.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0 || pane.getPages() == 1) return;

			pane.setPage(pane.getPage() - 1);

			forwardPane.setVisible(true);
			gui.update();
		});
		forwardPane.addItem(next, 0, 0);
		previousPane.addItem(previous, 0, 0);

		gui.addPane(forwardPane);
		gui.addPane(previousPane);
		gui.addPane(pane);
	}

	public ReportManageInventory(Reporter plugin, Player player, OfflinePlayer target) {
		super(plugin, player);
		this.gui = new Gui(plugin, 5, Translations.REPORT_MANAGE_MENU.get(player));
		gui.setOnGlobalClick(click -> click.setCancelled(true));
		OutlinePane oPane = new OutlinePane(0, 0, 9, 5);
		oPane.setRepeat(true);
		oPane.applyMask(new Mask(
				"111111111",
				"100000001",
				"100000001",
				"100000001",
				"111111111"));
		oPane.setOnClick(inventoryClickEvent -> inventoryClickEvent.setCancelled(true));

		oPane.addItem(new GuiItem(new ItemStackBuilder(XMaterial.WHITE_STAINED_GLASS_PANE.parseMaterial(true))
				.withName(" ")
				.addFlags(ItemFlag.HIDE_ATTRIBUTES)
				.build()));

		gui.addPane(oPane);

		PaginatedPane pane = new PaginatedPane(1, 2, 7, 1);
		List<ReportEntry> reports = new ArrayList<>(Reporter.getAPI().getReportCache().getReports());
		Collections.reverse(reports);
		List<GuiItem> guiItems = new ArrayList<>();
		reports.forEach(entry -> {
			OfflinePlayer reporter = entry.getReporter();
			OfflinePlayer reported = entry.getReported();
			if (reported.getUniqueId().equals(target.getUniqueId())) {
				String reason = entry.getReason();
				ItemStack item = new ItemStackBuilder(VersionUtil.getSkull(reported))
						.withLore(Arrays.asList(
								Translations.REPORT_REPORTER.get(player, reporter.getName()),
								Translations.REPORT_REPORTED.get(player, reporter.getName()),
								Translations.REPORT_REASON.get(player, reason),
								ChatColor.LIGHT_PURPLE + "" + entry.getDate(),
								entry.getStatus().getColour() + entry.getStatus().toString(),
								player.hasPermission("report.toggle") ? Translations.REPORT_TOGGLE.get(player) : "",
								player.hasPermission("report.remove") ? Translations.REPORT_REMOVE.get(player) : ""))
						.build();
				guiItems.add(new GuiItem(item, event -> {
					InventoryAction action = event.getAction();
					switch (action) {
						case PICKUP_HALF:
							if (!player.hasPermission("report.remove")) break;
							entry.deleteEntry();
							player.closeInventory();
							new ReportManageInventory(plugin, player).open();
							break;
						case PICKUP_ALL:
							if (!player.hasPermission("report.toggle")) break;
							entry.setStatus(entry.getStatus().toggle());
							player.closeInventory();
							new ReportManageInventory(plugin, player).open();
							break;
					}
				}));
			}
		});

		pane.populateWithGuiItems(guiItems);

		StaticPane forwardPane = new StaticPane(5, 4, 1 ,1);
		StaticPane previousPane = new StaticPane(3, 4, 1, 1);
		GuiItem next = new GuiItem(new ItemStackBuilder(XMaterial.ARROW.parseMaterial(true))
				.withName(ChatColor.GREEN + "Next Page")
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0 || pane.getPages() == 1) return;

			pane.setPage(pane.getPage() + 1);

			previousPane.setVisible(true);
			gui.update();
		});
		GuiItem previous = new GuiItem(new ItemStackBuilder(XMaterial.ARROW.parseMaterial(true))
				.withName(ChatColor.RED + "Previous Page")
				.build(), event -> {
			event.setCancelled(true);
			if (pane.getPages() == 0 || pane.getPages() == 1) return;

			pane.setPage(pane.getPage() - 1);

			forwardPane.setVisible(true);
			gui.update();
		});
		forwardPane.addItem(next, 0, 0);
		previousPane.addItem(previous, 0, 0);

		gui.addPane(forwardPane);
		gui.addPane(previousPane);
		gui.addPane(pane);
	}

	@Override
	public void open() {
		gui.show(getPlayer());
	}
}
