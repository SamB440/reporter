package net.islandearth.reporter.entry;

import net.islandearth.reporter.Reporter;
import net.islandearth.reporter.utils.FileUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ReportEntry {

	private final UUID uuid;
	private final OfflinePlayer reporter;
	private final OfflinePlayer reported;
	private final String reason;
	private final String date;
	private EntryStatus status;

	public UUID getUuid() {
		return uuid;
	}

	public OfflinePlayer getReporter() {
		return reporter;
	}

	public OfflinePlayer getReported() {
		return reported;
	}

	public String getReason() {
		return reason;
	}

	public String getDate() {
		return date;
	}

	public EntryStatus getStatus() {
		return status;
	}

	public void setStatus(EntryStatus status) {
		this.status = status;
	}

	public ReportEntry(OfflinePlayer reporter, OfflinePlayer reported, String reason, String date, EntryStatus status) {
		this.uuid = UUID.randomUUID();
		this.reporter = reporter;
		this.reported = reported;
		this.reason = reason;
		this.date = date;
		this.status = status;
	}

	public ReportEntry(UUID uuid, OfflinePlayer reporter, OfflinePlayer reported, String reason, String date, EntryStatus status) {
		this.uuid = uuid;
		this.reporter = reporter;
		this.reported = reported;
		this.reason = reason;
		this.date = date;
		this.status = status;
	}


	public void saveEntry() {
		Reporter plugin = Reporter.getAPI();
		if (!plugin.getReportCache().getReports().contains(this)) {
			plugin.getReportCache().getReports().add(this);
		}
		
		if (plugin.isSql()) {
			Connection sql = plugin.getSql();
			try {
				PreparedStatement p1 = sql.prepareStatement("SELECT * FROM Reports WHERE uuid = ?");
				p1.setString(1, getDbUuid(uuid));
				ResultSet rs = p1.executeQuery();
				boolean exists = false;
				while (rs.next()) {
					exists = true;
				}
				
				String statement = "INSERT INTO Reports (uuid, date, reporter, reported, reason, status) VALUES (?, ?, ?, ?, ?)";
				if (exists) statement = "UPDATE Reports SET uuid = ?, date = ?, reporter = ?, reported = ?, reason = ?, status = ? WHERE uuid = ?";
				PreparedStatement p2 = sql.prepareStatement(statement);
				p2.setString(1, getDbUuid(uuid));
				p2.setString(2, date);
				p2.setString(3, reporter.getUniqueId().toString());
				p2.setString(4, reported.getUniqueId().toString());
				p2.setString(5, reason);
				p2.setString(6, status.toString());
				if (exists) p2.setString(7, getDbUuid(uuid));
				p2.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			File logger = new File(plugin.getDataFolder() + "/data/logger.yml");
			FileUtil config = new FileUtil(logger);
			config.save("Reports." + uuid + ".reporter", reporter.getUniqueId().toString());
			config.save("Reports." + uuid + ".reported", reported.getUniqueId().toString());
			config.save("Reports." + uuid + ".reason", reason);
			config.save("Reports." + uuid + ".status", status.toString());
			config.save("Reports." + uuid + ".date", date);
			config.exit();
		}
	}
	
	public void deleteEntry() {
		Reporter plugin = Reporter.getAPI();
		plugin.getReportCache().getReports().remove(this);
		
		if (plugin.isSql()) {
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				Connection sql = plugin.getSql();
				try {
					PreparedStatement p1 = sql.prepareStatement("DELETE FROM Reports WHERE uuid = ?");
					p1.setString(1, getDbUuid(uuid));
					p1.executeUpdate();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
		} else {
			File logger = new File(plugin.getDataFolder() + "/data/logger.yml");
			FileUtil config = new FileUtil(logger);
			config.getConfig().set("Reports." + uuid, null);
			config.exit();
		}
	}

	private String getDbUuid(UUID uuid) {
		return uuid.toString();
	}

	public enum EntryStatus {
		OPEN(ChatColor.GREEN),
		CLOSED(ChatColor.RED);

		private ChatColor colour;
		
	    public EntryStatus toggle() {
	        return values()[~ordinal() & 1];
	    }

		public ChatColor getColour() {
			return colour;
		}

		EntryStatus(ChatColor colour) {
	    	this.colour = colour;
		}
	}
}
