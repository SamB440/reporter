package net.islandearth.reporter.utils;

import net.islandearth.reporter.entry.ReportEntry;

import java.util.ArrayList;
import java.util.List;

public class ReportCache {

	private List<ReportEntry> reports = new ArrayList<>();

	public List<ReportEntry> getReports() {
		return reports;
	}
}
