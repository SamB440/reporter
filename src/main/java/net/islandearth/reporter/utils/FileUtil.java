package net.islandearth.reporter.utils;

import net.islandearth.reporter.Reporter;
import net.islandearth.reporter.entry.ReportEntry;
import net.islandearth.reporter.entry.ReportEntry.EntryStatus;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FileUtil {

	private File file;
	private FileConfiguration config;

	public File getFile() {
		return file;
	}

	public FileConfiguration getConfig() {
		return config;
	}

	public FileUtil(File file) {
		this.file = file;
		this.config = YamlConfiguration.loadConfiguration(file);
	}
	
	public void save(String path, Object data) {
		config.set(path, data);
	}
	
	public static List<ReportEntry> getReports() {
		Reporter plugin = Reporter.getAPI();
		List<ReportEntry> reports = new ArrayList<>();
		if (plugin.isSql()) {
			Connection sql = plugin.getSql();
			try {
				PreparedStatement p1 = sql.prepareStatement("SELECT * FROM Reports");
				ResultSet rs = p1.executeQuery();
				while (rs.next()) {
					UUID uuid = UUID.fromString(rs.getString("uuid"));
					OfflinePlayer reporter = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("reporter")));
					OfflinePlayer reported = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("reported")));
					String reason = rs.getString("reason");
					EntryStatus status = EntryStatus.valueOf(rs.getString("status"));
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
					LocalDateTime time = LocalDateTime.parse(rs.getString("date").replace("T", " "), formatter);
					reports.add(new ReportEntry(uuid, reporter, reported, reason, time.format(formatter), status));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			FileConfiguration config = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + "/data/logger.yml"));
			if (config.getConfigurationSection("Reports") != null) {
				config.getConfigurationSection("Reports").getKeys(false).forEach(path -> {
					ConfigurationSection section = config.getConfigurationSection("Reports." + path);
					OfflinePlayer reporter = Bukkit.getOfflinePlayer(UUID.fromString(section.getString("reporter")));
					OfflinePlayer reported = Bukkit.getOfflinePlayer(UUID.fromString(section.getString("reported")));
					String reason = section.getString("reason");
					EntryStatus status = EntryStatus.valueOf(section.getString("status"));
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");
					String date = section.getString("date");
					LocalDateTime time = LocalDateTime.parse(date.replace("T", " "), formatter);
					reports.add(new ReportEntry(UUID.fromString(path), reporter, reported, reason, time.format(formatter), status));
				});
			}
		}
		return reports;
	}
	
	public void exit() {
		saveConfig();
		this.file = null;
		this.config = null;
	}
	
	private void saveConfig() {
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
